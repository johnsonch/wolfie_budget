Rails.application.routes.draw do

  resources :budget_entries
  resources :budget_categories
  resources :categories
  resources :users do
    resources :budgets
  end

  get 'about'      => 'static_pages#about'
  get 'opensource' => 'static_pages#opensource'
  get 'signup'     => 'users#new'
  
  get    'login'  => 'sessions#new'
  post   'login'  => 'sessions#create'
  delete 'logout' => 'sessions#destroy'
  
  root 'static_pages#home'
end
