require 'test_helper'

class BudgetsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
    @budget = budgets(:one)
    @budget.user = @user 
  end

  test "should get index" do
    get user_budgets_url(user_id: @budget.user_id)
    assert_response :success
  end

  test "should get new" do
    get new_user_budget_url(user_id: @budget.user_id)
    assert_response :success
  end

  test "should create budget" do
    assert_difference('Budget.count') do
      post user_budgets_url(user_id: @budget.user_id), params: { budget: { month: @budget.month, notes: @budget.notes, user_id: @budget.user_id, year: @budget.year } }
    end

    assert_redirected_to user_budget_url(id: Budget.last, user_id: Budget.last.user_id)
  end

  test "should show budget" do
    get user_budget_url(id: @budget.id, user_id: @budget.user_id)
    assert_response :success
  end

  test "should get edit" do
    get edit_user_budget_url(id: @budget.id, user_id: @budget.user_id)
    assert_response :success
  end

  test "should update budget" do
    patch user_budget_url(id: @budget.id, user_id: @budget.user_id), params: { budget: { month: @budget.month, notes: @budget.notes, user_id: @budget.user_id, year: @budget.year } }
    assert_redirected_to user_budget_url(@budget.id)
  end

  test "should destroy budget" do
    assert_difference('Budget.count', -1) do
      delete user_budget_url(id: @budget.id, user_id: @budget.user_id)
    end

    assert_redirected_to user_budgets_url(user_id: @budget.user_id)
  end
end
