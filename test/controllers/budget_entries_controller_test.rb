require 'test_helper'

class BudgetEntriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @budget_entry = budget_entries(:one)
    @budget_entry.budget_category = budget_categories(:one)
  end

  test "should get index" do
    get budget_entries_url
    assert_response :success
  end

  test "should get new" do
    get new_budget_entry_url
    assert_response :success
  end

  test "should create budget_entry" do
    assert_difference('BudgetEntry.count') do
      post budget_entries_url, params: { budget_entry: { amount: @budget_entry.amount, budget_category_id: @budget_entry.budget_category_id, notes: @budget_entry.notes } }
    end

    assert_redirected_to budget_entry_url(BudgetEntry.last)
  end

  test "should show budget_entry" do
    get budget_entry_url(@budget_entry)
    assert_response :success
  end

  test "should get edit" do
    get edit_budget_entry_url(@budget_entry)
    assert_response :success
  end

  test "should update budget_entry" do
    patch budget_entry_url(@budget_entry), params: { budget_entry: { amount: @budget_entry.amount, budget_category_id: @budget_entry.budget_category_id, notes: @budget_entry.notes } }
    assert_redirected_to budget_entry_url(@budget_entry)
  end

  test "should destroy budget_entry" do
    assert_difference('BudgetEntry.count', -1) do
      delete budget_entry_url(@budget_entry)
    end

    assert_redirected_to budget_entries_url
  end
end
