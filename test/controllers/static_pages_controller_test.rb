require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  test "should have a root url" do
    get root_path
    assert_response :success
  end

  test "should get about" do
    get about_path
    assert_response :success
  end

  test "should get opensource" do
    get opensource_path
    assert_response :success
  end

end
