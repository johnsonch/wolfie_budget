require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(first_name: "Bart",
                     last_name: "Simpson",
                     email: "bart@simpsons.com",
                     password: 'foobar',
                     password_confirmation: 'foobar')
  end

  test "should be valid" do
    assert @user.valid?
  end
end
