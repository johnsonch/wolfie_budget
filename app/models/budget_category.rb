class BudgetCategory < ApplicationRecord
  belongs_to :budget
  has_many :budget_entries
end
