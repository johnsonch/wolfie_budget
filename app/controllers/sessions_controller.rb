class SessionsController < ApplicationController
  def new
  end
  
  def create
    user = User.find_by(email: params[:session][:email])
    if user && user.authenticate(params[:session][:password])
      # login the user
      log_in(user)
      # sent to their dashboard
      redirect_to(user)
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new' # to save a missing template error
    end
  end
  
  def destroy
    log_out
    redirect_to root_url
  end
end
