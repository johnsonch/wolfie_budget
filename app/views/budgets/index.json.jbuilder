json.array!(@budgets) do |budget|
  json.extract! budget, :id, :user_id, :year, :month, :notes
  json.url budget_url(budget, format: :json)
end
