json.array!(@budget_entries) do |budget_entry|
  json.extract! budget_entry, :id, :budget_category_id, :amount, :notes
  json.url budget_entry_url(budget_entry, format: :json)
end
