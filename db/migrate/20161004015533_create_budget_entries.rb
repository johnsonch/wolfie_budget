class CreateBudgetEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :budget_entries do |t|
      t.integer :budget_category_id
      t.float :amount
      t.text :notes

      t.timestamps
    end
  end
end
