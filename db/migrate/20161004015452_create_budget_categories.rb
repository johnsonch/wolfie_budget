class CreateBudgetCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :budget_categories do |t|
      t.integer :budget_id
      t.string :category_name
      t.float :category_amount

      t.timestamps
    end
  end
end
