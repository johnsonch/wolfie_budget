class CreateBudgets < ActiveRecord::Migration[5.0]
  def change
    create_table :budgets do |t|
      t.integer :user_id
      t.integer :year
      t.string :month
      t.text :notes

      t.timestamps
    end
  end
end
